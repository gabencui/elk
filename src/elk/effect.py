# This file is part of elk.
# 
# elk is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# elk is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# elk.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from .base import ElkBaseClass, AudioFrame, Oscillator

from types import FunctionType
from collections.abc import Iterator

class Effect(ElkBaseClass):
    """Effect class for inheritance."""

    def __init__(self, *default_args, chunksize: float = 64,
                 overhead: float = 0, average: bool = False, numchannels=1,
                 **kwargs) -> None:
        """Create an effect."""
        ElkBaseClass.__init__(self, **kwargs)

        self.chunksize = chunksize
        self.overhead = overhead
        self.average = average
        self.numchannels = numchannels
        self._default_args = self.parse_args(default_args)

        self.setup()

    def setup(self) -> None:
        pass
    def function(self, chunk, *args, **kwargs) -> None:
        return chunk
    def __repr__(self) -> str:
        return ("{name}({_default_args}))".format(
                    **{**self.__dict__, 'name': self.__class__.__name__}))
    @property
    def params(self) -> str:
        return {'chunksize': self.chunksize,
                'overhead': self.overhead,
                'average': self.average}
    
    def parse_args(self, args: tuple) -> tuple:
        from itertools import repeat
    
        return (*(arg if isinstance(arg, Iterator) else repeat(arg)
                  for arg in (args if isinstance(args, tuple) else
                              (args,))),)
    def __call__(self, frame: AudioFrame, *args, **kwargs) -> AudioFrame:
        """Process and return a frame.
    
        If *args is given, each arg overrides default_args in succession.
    
        """
        args = (*self.parse_args(args), *self._default_args[len(args):])
        array_ = np.array([[]])
    
        kwargs = {**self.params, **kwargs}
        chunksize, overhead = kwargs['chunksize'], kwargs['overhead']
        average = kwargs['average']
    
        from itertools import islice
        for chunk in (frame.array[:, i:(i + chunksize + overhead)]
                      for i in range(0, frame.length, chunksize)):
            args_ = [
                sum(islice(arg, chunksize)) / chunksize if average else
                next(islice(arg, chunksize - 1, chunksize))
                for arg in args]
            array_ = np.hstack((array_, self.process(
                chunk,
                *args_,
                **{**kwargs,
                   'properties': frame.properties})[:, :chunksize]))
    
        return AudioFrame(array_, **frame.properties)

from scipy.signal import lfilter, zpk2tf
class LPF(Effect):
    def setup(self):
        self.zf = np.zeros((1, 1))

    def function(self, chunk, cutoff, r, **kwargs):
        cutoff /= 2 * self.rate
        p1 = r * np.exp(2j * np.pi * cutoff)
        p2 = p1.conj()
        z1 = -r
        k = 1
        b, a = zpk2tf([p1, p2], [], [k])

        zshape = (chunk.shape[0], 2)
        if self.zf.shape != zshape:
            self.zf = np.zeros(zshape)

        out, self.zf = lfilter(b, a, chunk, zi=self.zf)
        return out

from scipy.interpolate import interp1d

class SimpleDelay(Effect):
    def setup(self):
        self.ringbuffer = np.zeros((self.numchannels, 2))
        self.head = 0

    def process(self, chunk, time, feedback, **kwargs):
        if self.ringbuffer.shape[1] != time * self.rate:
            new_len = max(int(time * self.rate), chunk.shape[1])
            prev_len = self.ringbuffer.shape[1]
            interp = interp1d(np.hstack((np.linspace(0, 1, prev_len),) *
                                        self.numchannels),
                              self.ringbuffer, kind='nearest')
            self.ringbuffer = interp(np.linspace(0, 1, new_len))
            self.head = int(self.head * new_len / prev_len)

        overflow = max(0, self.head + chunk.shape[1] -
                       self.ringbuffer.shape[1])
        end = self.head + chunk.shape[1] - overflow
        buffered = np.hstack((
            self.ringbuffer[:, self.head:end],
            self.ringbuffer[:, :overflow]))

        #print(buffered, overflow, self.head, end)
        out = chunk + feedback * buffered

        self.ringbuffer[:, self.head:end] = out[:, :(-overflow if overflow else None)]
        if overflow:
            self.ringbuffer[:, :overflow] = out[:, -overflow:]
        self.head = (self.head + chunk.shape[1]) % self.ringbuffer.shape[1]

        return out
