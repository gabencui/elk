# <<License>>

from .base import (set_rate, set_bpm, set_sig, get_rate, get_bpm, get_sig,
                   AudioFrame, Oscillator, osc, Envelope, OscillatorCollection,
                   Waveform, sin, sqr, tri, saw, sinz, sqrz, triz, sawz,
                   noise, noisez, const)
from .sequencing import (Soundbank, Sequencer, Pattern)
from .effect import (Effect, SimpleDelay)
